// Load dependencies
const autoprefixer = require('gulp-autoprefixer');
const gulp = require('gulp');
const babel = require ('gulp-babel');
const cleanCss = require('gulp-clean-css');
const concat = require ('gulp-concat');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');

// Define source files
const source = {
    sass: './sass/style.scss',
    script: './js/main.js',
};

// Define output directories
const output = {
    sass: './dist',
    scripts: './dist',
};

// Autoprefixer settings
const autoprefixerBrowsers = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

// Compile and automatically prefix stylesheets
gulp.task('sass', () => {
    return gulp.src(source.sass)
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer('last 2 versions'))
        .pipe(cleanCss({compatibility: 'ie8'}))
        .pipe(concat('main.css'))
        .pipe(gulp.dest(output.sass))
});

// Process all custom Javascript files
gulp.task('script', () => {
    return gulp.src(source.script)
        .pipe(plumber())
        .pipe(babel())
        .pipe(uglify())
        .pipe(concat('main.js'))
        .pipe(gulp.dest(output.scripts));
});

// Builds assets
gulp.task('build', ['sass', 'script'], () => {
    console.log('Gulp build is done!');
});
   
// Initialize default task
gulp.task('default', ['build']);