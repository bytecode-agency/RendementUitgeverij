// Import styling for Parcel Bundler
import '../sass/style.scss';
import 'jquery'; // eslint-disable-line import/first
import 'popper.js'; // eslint-disable-line import/first
import 'bootstrap'; // eslint-disable-line import/first

const $ = require('jquery');
require('jquery.nicescroll');

/* global $ window document */
$(document).ready(function () { // eslint-disable-line

    // Mobile menu
    $('#hamburger, #overlay a').click(function () {
        $('#hamburger').toggleClass('open');
        $('#overlay').toggleClass('open');
        $('.menu-bar.mobile.scrolled').toggleClass('no-bg');
    });

    // Menu styling when scrolling
    $(window).scroll(function () {
        if ($(document).width() >= 768) { // Initialize parallax if not on mobile
            parallax(); // eslint-disable-line
        }
        const scroll = $(window).scrollTop();
        if (scroll > 300) {
            $('.menu-bar').addClass('scrolled');
        } else {
            $('.menu-bar').removeClass('scrolled');
        }
    });

    // FAQ
    if ($('.toggle .toggle-title').hasClass('active')) {
        $('.toggle .toggle-title.active')
            .closest('.toggle')
            .find('.toggle-inner')
            .show();
    }

    $('.toggle .toggle-title').click(function () {
        if ($(this).hasClass('active')) {
            $(this)
                .removeClass('active')
                .closest('.toggle')
                .find('.toggle-inner')
                .slideUp(200);
        } else {
            $('.toggle-title.active').removeClass('active');
            $('.toggle .toggle-inner').slideUp(200);
            $(this)
                .addClass('active')
                .closest('.toggle')
                .find('.toggle-inner')
                .slideDown(200);
        }
    });

    // Smooth scrolling
    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 100,
        }, 400);
    });

    // Scroll to top when clicking on logo
    $(document).on('click', '.logo', function (event) { // eslint-disable-line
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0,
        }, 400);
    });


    // Add parallax scrolling
    function parallax() {
        const addParallax = $('.intro');
        const yPos = -($(window).scrollTop() / 1.1);
        const coords = `center ${yPos}px`;
        // const scale = `${100 - (0.1 * yPos)}% auto`;
        $(addParallax).css({
            backgroundPosition: coords,
            // backgroundSize: scale,
        });

        // Too bad there is not a shame.js file...
        // $('head').append(`<style>.intro::before{ background: rgba(0, 0, 0, ${-yPos / 250}); }</style>`);
    }

    // Set correct positions of parallax on page load
    if ($(document).width() >= 768) {
        parallax();
    }

    // Make page scrolling smoother
    // $('body').niceScroll();
});
