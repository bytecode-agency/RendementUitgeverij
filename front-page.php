<?php 
/*
Template Name: Homepage
Template Post Type: page
*/

get_header();

require_once('home-partials/intro.php');
require_once('home-partials/about.php');
require_once('home-partials/diensten.php');
require_once('home-partials/contact.php');
require_once('home-partials/faq.php');
require_once('home-partials/ads.php');
require_once('home-partials/jobs.php');

get_sidebar();
get_footer();

?>