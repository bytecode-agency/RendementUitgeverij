<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="tekst">
    <meta name="keywords" content="tekst">
    <title>Rendement Uitgeverij</title>

    <!-- Third party dependencies from CDN -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    <!-- Custom script -->
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri() . '/' ?>dist/main.css">
</head>

<body>
    <!-- Logo and mobile menu -->
    <div class="menu-bar mobile">
        <div class="container intro__logo animated fadeInDown delay-1s mobile">
            <img src="<?= get_stylesheet_directory_uri() . '/' ?>images/logo-white.png" class="logo" alt="Logo">
            <div class="wrapper-menu" id="hamburger">
                <div class="line-menu half start"></div>
                <div class="line-menu"></div>
                <div class="line-menu half end"></div>
            </div>
        </div>
    </div>

    <!-- Logo and desktop menu -->
    <div class="menu-bar desktop">
        <div class="container intro__logo animated fadeInDown delay-1s">
            <img src="<?= get_stylesheet_directory_uri() . '/' ?>images/logo-white.png" class="logo" alt="Logo">
            <nav class="navbar">
                <a href="#rendement-uitgeverij" class="navbar__item">Rendement Uitgeverij</a>
                <a href="#producten" class="navbar__item">Producten</a>
                <a href="#service-contact" class="navbar__item">Service & Contact</a>
                <a href="#veelgestelde-vragen" class="navbar__item">Veelgestelde Vragen</a>
                <a href="#adverteren" class="navbar__item">Adverteren</a>
                <a href="#werken-bij" class="navbar__item">Werken bij Rendement</a>
            </nav>
        </div>
    </div>

    <!-- Mobile menu overlay -->
    <div class="overlay" id="overlay">
        <nav class="overlay-menu">
            <ul>
                <li>
                    <a href="#rendement-uitgeverij">Rendement Uitgeverij</a>
                </li>
                <li>
                    <a href="#producten">Producten</a>
                </li>
                <li>
                    <a href="#service-contact">Service & Contact</a>
                </li>
                <li>
                    <a href="#veelgestelde-vragen">Veelgestelde Vragen</a>
                </li>
                <li>
                    <a href="#adverteren">Adverteren</a>
                </li>
                <li>
                    <a href="#werken-bij">Werken bij Rendement</a>
                </li>
            </ul>
        </nav>
    </div>