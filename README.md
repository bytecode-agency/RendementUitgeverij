# Rendement

[![Greenkeeper badge](https://badges.greenkeeper.io/NooijenSolutions/RendementUitgeverij.svg)](https://greenkeeper.io/)

[![Build Status](https://travis-ci.org/NooijenSolutions/RendementUitgeverij.svg?branch=master)](https://travis-ci.org/NooijenSolutions/RendementUitgeverij)
[![dependencies Status](https://david-dm.org/nooijensolutions/RendementUitgeverij/status.svg)](https://david-dm.org/nooijensolutions/RendementUitgeverij)
[![devDependencies Status](https://david-dm.org/nooijensolutions/RendementUitgeverij/dev-status.svg)](https://david-dm.org/nooijensolutions/RendementUitgeverij?type=dev)
[![GPL Licence](https://badges.frapsoft.com/os/gpl/gpl.svg?v=103)](https://opensource.org/licenses/GPL-3.0/)
[![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/)

This is the WordPress theme for Rendement Uitgeverij

## Dependencies

Development:

* Yarn (not NPM)
* Node

Production:

* WordPress server
  * PHP (5.6+)
  * MySQL or other WordPress compatible SQL server

To install all dependencies (needed for both development and production build), run:

```sh
yarn
```

## Development

For development, the Parcel Package Bundler is used. To run the development server, run the following command:

```sh
yarn run dev
```

The Parcel Bundler is now running a dev-server on localhost:1234

## Production build

For making the production build, run the following command:

```sh
yarn run build
```

For also building the HTML, run the following command

```sh
yarn run build:html
```

To build the HTML used for Github Pages, the files must be in the `dist` directory. This is automatically done with the following command 

```sh
yarn run build:docs
```

Or you can use Gulp for the normal production build:

```sh
yarn run build:gulp
```

If you want to use Gulp for the build process, make sure the `import 'dependency';` lines are removed from the main.js file, and you include the dependencies from the CDN or from another source, as Gulp does not bundle assets, while Parcel does. So in short: do yourself a favor by using Parcel over Gulp

When the theme is installed on the server, run the following command (while in the root directory of the theme):

```sh
sh production.sh && rm -f production.sh
```

**Make sure you have separate development and production environments!**

## Contributors

* Luciano Nooijen
* Jeroen van Steijn
