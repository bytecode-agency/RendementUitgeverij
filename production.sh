#!/bin/sh
rm -f dist/index.html dist/source.html .babelrc .esdoc.json .eslintrc .gitignore .postcssrc .travis.yml Gulpfile.js LICENSE.md package.json README.md source.html yarn-error.log yarn.lock
echo 'Cleaned dist folder and development files in foot folder'

rm -rf sass
echo 'Removed sass folder'

rm -rf js
echo 'Removed js folder'

rm -rf docs
echo 'Removed docs folder'

rm -rf .git
echo 'Removed .git folder'

echo "It's all clear. You're good to go!"
