    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row animated fadeIn footer-items">
                <ul class="col-lg-3 col-md-6 col-xs-12 footer-list">
                    <li>Over Rendement</li>
                    <li>
                        <a href="#">Over ons</a>
                    </li>
                    <li>
                        <a href="#">Producten</a>
                    </li>
                    <li>
                        <a href="#">Adverteren</a>
                    </li>
                    <li>
                        <a href="#">Werken bij Rendement</a>
                    </li>
                    <br>
                    <li>
                        <a href="#">Foto's door Antim Photography</a>
                    </li>
                </ul>
                <ul class="col-lg-3 col-md-6 col-xs-12 footer-list">
                    <li>Service & Contact</li>
                    <li>
                        <a href="#">Over ons</a>
                    </li>
                    <li>
                        <a href="#">Mijn account</a>
                    </li>
                    <li>
                        <a href="#">Routebeschrijving</a>
                    </li>
                </ul>
                <ul class="col-lg-3 col-md-6 col-xs-12 footer-list">
                    <li>Evenementen</li>
                    <li>
                        <a href="#">Congressen</a>
                    </li>
                    <li>
                        <a href="#">Opleidingen</a>
                    </li>
                </ul>
                <ul class="col-lg-3 col-md-6 col-xs-12 footer-list gray-list">
                    <li class="black">Rendement Uitgeverij BV.</li>
                    <li>Conradstraat 38</li>
                    <li>Postbus 27020</li>
                    <li>3003 LA Rotterdam</li>
                    <br/>
                    <li>T: 010 - 2433933</li>
                    <li>T: 010 - 2439028</li>
                </ul>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <img src="<?= get_stylesheet_directory_uri() . '/' ?>images/logo-black-footer.png" alt="Rendement Uitgeverij">
                    </div>
                    <div class="col-lg-8 col-md-6 copyright-notice">
                        <div>Gebruik van deze site betekent dat u onze
                            <a href="#">algemene voorwaarden</a> accepteert. - cookies:
                            <a href="#">informatie</a>
                        </div>
                        <div>kvk: 27181487 | IBAN: nl24 ingb 0006 4159 44 | btw: nl 8079.64.712.b.01</div>
                        <div>copyright &copy; 2009 -
                            <script>
                                document.write(new Date().getFullYear())
                            </script> - rendement.nl</div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Custom script -->
    <script src="<?= get_stylesheet_directory_uri() . '/' ?>dist/main.js"></script>
</body>

</html>