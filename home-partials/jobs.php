<!-- Werken bij -->
<section class="jobs" id="werken-bij">
    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-9 col-lg-7">
                <h1>Werken bij Rendement</h1>
            </div>
            <div class="col-sm-12 col-md-9 col-lg-7">
                <p>Werken bij Rendement betekent een baan binnen een organisaqtie waar ruimte is voor eigen initiatief. Rendement biedt zowel aan starters, jonge professionals als de ervaren professional de mogelijkheid om te groeien. Binnen Rendement, maar ook binnen de wereldwijde Springer Groep.</p>
                <div class="vacature-link">
                    <a href="#">Vacature Medewerker klantenservice en abonnementenadministratie</a>
                </div>
            </div>
        </div>
    </div>
</section>