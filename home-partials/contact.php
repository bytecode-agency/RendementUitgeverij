<!-- Service contact -->
<section class="service-contact" id="service-contact">
    <div class="container animated fadeIn">
        <h1>Service & Contact</h1>
        <p>Rendement Uitgeverij BV.</p>
        <p>Conradstraat 38</p>
        <p>Postbus 27020</p>
        <p>3003 LA Rotterdam</p>
        <p>T: 010-2433933</p>
        <p>F: 010-2439028</p>
    </div>
</section>