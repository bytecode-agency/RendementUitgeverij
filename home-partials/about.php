<!-- About -->
<section class="about" id="rendement-uitgeverij">
    <div class="container animated fadeIn">
        <h1>Rendement Uitgeverij</h1>
        <p>
            Rendement Uitgeverij is een multimediale uitgever van vakinformatie. Een echte Rotterdamse uitgeverij waar medewerkers hun handen uit de mouwen steken. Met zestig man werken we aan mooie producten en diensten die u helpen in uw vakgebied. En dat doen we al meer dan 15 jaar. Inmiddels informeren wij tienduizenden professionals over belangrijke wet- en regelgeving op hun vakgebied. Op een heldere en toegankelijke manier, tegen een eerlijke prijs.
        </p>
        <p>
            Bij Rendement maken we vakbladen, pockets, online content, zoals naslagwerken en tools, nieuwsbrieven en opleidingen & congressen voor professionals in elf vakgebieden. Van personeelszaken tot financiën, van fiscaal tot arbo. We zitten bovenop nieuwe ontwikkelingen in wet- en regelgeving en op momenten als Prinsjesdag bestellen we pizza en maken we flink wat overuren om te zorgen dat u zo snel mogelijk op de hoogte bent. Maar daar proosten we dan ook wel op aan het einde van de week.
        </p>
    </div>
</section>