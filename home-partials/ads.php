<!-- Adverteren -->
<section class="rendement-ads" id="adverteren">
    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-md-8">
                <img class="inline-logo" src="<?= get_stylesheet_directory_uri() . '/' ?>images/rendement-adverteren.png" alt="Adverteren" width="80%"></img>
                <br/>
                <br/>
                <p>Rendement Uitgeverij biedt u verschillende mogelijkheden om uw product en/of dienst onder de aandachtt te brengen van vakprofessionals in het mkb en non-profit organisaties. Een unieke formule van onder andere 11 vakbladen en themadossiers maakt het mogelijk om per doelgroep een gerichte boodschap te communiceren. Zo bereikt u uw doelgroep(en) op een zeer effectieve manier.</p>
                <h2>Tariefinformatie</h2>
                <p>Onderstaand vindt u alle gegevens over advertentietarieven, sluitingsdata en advertentiemateriaal. Klik op een van de onderstaande links om een van de (pdf) documenten met tariefinformatie te openen.</p>
            </div>
            <div class="col-md-4">
                <ul class="pdf-downloads">
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart Arbo Rendement</a>
                    </li>
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart Bestuur Rendement</a>
                    </li>
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart BV Rendement</a>
                    </li>
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart FA Rendement</a>
                    </li>
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart Fiscaal Rendement</a>
                    </li>
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart HR Rendement</a>
                    </li>
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart Marketing Rendement</a>
                    </li>
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart MT Rendement</a>
                    </li>
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart Office Rendement</a>
                    </li>
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart OR Rendement</a>
                    </li>
                    <li>
                        <a href="#" class="pdf-link">Tariefkaart Salaris Rendement</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>