<!-- FAQ -->
<section class="faq" id="veelgestelde-vragen">
    <div class="container animated fadeIn">
        <h1 class="white faq__title">Veelgestelde vragen</h1>
        <p>Heeft u een vraag over de dienstverlening van Rendement Uitgeverij? Graag helpen wij u aan een passend antwoord. U kunt hieronder de veelgestelde vragen raadplegen. Staat het antwoord op uw vraag er niet bij, neem dan gerust contact met ons op.</p>
        <div class="faq-dropdowns">
            <div class="toggle">
                <div class="toggle-title">
                    Nieuwsbrieven
                </div>
                <div class="toggle-inner">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam vel rerum in natus quo delectus eos est, repellendus, magnam molestias doloremque odit fuga aliquid perspiciatis, dolor alias laboriosam sed ut dolorem molestiae. Voluptatibus dicta rem ex quas corrupti? Ab, quaerat?
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title">
                    Redactie
                </div>
                <div class="toggle-inner">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam vel rerum in natus quo delectus eos est, repellendus, magnam molestias doloremque odit fuga aliquid perspiciatis, dolor alias laboriosam sed ut dolorem molestiae. Voluptatibus dicta rem ex quas corrupti? Ab, quaerat?
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title">
                    Adverteren in Rendement
                </div>
                <div class="toggle-inner">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam vel rerum in natus quo delectus eos est, repellendus, magnam molestias doloremque odit fuga aliquid perspiciatis, dolor alias laboriosam sed ut dolorem molestiae. Voluptatibus dicta rem ex quas corrupti? Ab, quaerat?
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title">
                    Nieuwsbrieven
                </div>
                <div class="toggle-inner">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam vel rerum in natus quo delectus eos est, repellendus, magnam molestias doloremque odit fuga aliquid perspiciatis, dolor alias laboriosam sed ut dolorem molestiae. Voluptatibus dicta rem ex quas corrupti? Ab, quaerat?
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title">
                    Redactie
                </div>
                <div class="toggle-inner">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam vel rerum in natus quo delectus eos est, repellendus, magnam molestias doloremque odit fuga aliquid perspiciatis, dolor alias laboriosam sed ut dolorem molestiae. Voluptatibus dicta rem ex quas corrupti? Ab, quaerat?
                </div>
            </div>
            <div class="toggle">
                <div class="toggle-title">
                    Adverteren in Rendement
                </div>
                <div class="toggle-inner">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam vel rerum in natus quo delectus eos est, repellendus, magnam molestias doloremque odit fuga aliquid perspiciatis, dolor alias laboriosam sed ut dolorem molestiae. Voluptatibus dicta rem ex quas corrupti? Ab, quaerat?
                </div>
            </div>
        </div>
    </div>
</section>