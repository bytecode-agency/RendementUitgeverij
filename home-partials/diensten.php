<!-- Diensten -->
<section class="dienst-icons" id="producten">
    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-xs-12 dienst">
                <img src="<?= get_stylesheet_directory_uri() . '/' ?>images/vakbladen.png" class="dienst__icon" alt="Dienst"></img>
                <h2 class="dienst__title">Vakbladen</h2>
                <p class="dienst__text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                <a href="#" class="dienst__button">Meer informatie</a>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12 dienst">
                <img src="<?= get_stylesheet_directory_uri() . '/' ?>images/boeken.png" class="dienst__icon" alt="Dienst"></img>
                <h2 class="dienst__title">Boeken</h2>
                <p class="dienst__text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                <a href="#" class="dienst__button">Meer informatie</a>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12 dienst">
                <img src="<?= get_stylesheet_directory_uri() . '/' ?>images/online.png" class="dienst__icon" alt="Dienst"></img>
                <h2 class="dienst__title">Online</h2>
                <p class="dienst__text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                <a href="#" class="dienst__button">Meer informatie</a>
            </div>
            <div class="col-lg-3 col-md-6 col-xs-12 dienst">
                <img src="<?= get_stylesheet_directory_uri() . '/' ?>images/opleidingen-congressen.png" class="dienst__icon" alt="Dienst"></img>
                <h2 class="dienst__title">Opleidingen & congressen</h2>
                <p class="dienst__text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.</p>
                <a href="#" class="dienst__button">Meer informatie</a>
            </div>
        </div>
    </div>
</section>